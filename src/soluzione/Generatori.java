package soluzione;

public class Generatori extends Nodi {

	static private int nIstanzeGen;
	//listaConnessi contiene i Trasformatori connessi a questo Nodo
	public Generatori(int capac) {
		super(capac);
		setNIstanzeGen(getNIstanzeGen() + 1);		
	}	

	public static int getNIstanzeGen() {
		return nIstanzeGen;
	}

	public static void setNIstanzeGen(int nIstanzeGen) {
		Generatori.nIstanzeGen = nIstanzeGen;
	}

}
