/**
 * 
 */
package soluzione;

import java.util.LinkedList;

/**
 * @author Alessandro Buggin
 * @author Andrea Chiericati
 */
public abstract class Nodi {
	LinkedList<String> take;
	LinkedList<String> give;
	final int _capac;
	int liberi;//capacita' disponibile per le utenze (rimanente)

	public Nodi(){
		this(0);
	}
	
	public Nodi(int capac){
		take = new LinkedList<String>();
		give = new LinkedList<String>();
		_capac=capac;
		liberi=capac;
	}
}