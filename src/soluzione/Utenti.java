package soluzione;

public class Utenti extends Nodi {

	private int nIstanzeUtenti;

	public Utenti() {
		super(0);
		setnIstanzeUtenti(getnIstanzeUtenti() + 1);
	}

	public int getnIstanzeUtenti() {
		return nIstanzeUtenti;
	}

	public void setnIstanzeUtenti(int nIstanzeUtenti) {
		this.nIstanzeUtenti = nIstanzeUtenti;
	}

	/**
	 * controlla se questo Utente sia collegato a piu' Trasmettitori
	 */
	public boolean hasMultiTra(){
		//System.out.println("DEBUG: inizio uteCor.hasMultiTra ");
		if(take.size()<=1)
			return false;
		else
			return true;
	}
}
