package soluzione;

/**
 * @author Alessandro Buggin
 * @author Andrea Chiericati
 */
public class MalformedFileException extends Exception {
	private static final long serialVersionUID = 6903749794507613061L;

	public MalformedFileException(String err){
		System.err.println("File malformato: \n "+err);
	}
}
