package soluzione;

public class Trasmettitori extends Nodi {

	static private int nIstanzeTra;//Rete.trha.size();
	//listaConnessi contiene gli Utenti e i Trasmettitori connessi a questo Nodo
	
	public Trasmettitori(int capac) {
		super(capac);
		setNIstanzeTra(getNIstanzeTra() + 1);		
	}

	public static int getNIstanzeTra() {
		return nIstanzeTra;
	}

	public static void setNIstanzeTra(int nIstanzeTra) {
		Trasmettitori.nIstanzeTra = nIstanzeTra;
	}

	/**
	 * Ha un numero di Utenti maggiore della propria capacita'.
	 * Si avvale del supporto di un nodo Pericoloso. 
	 */
	public boolean isSovraccarico(){
		//System.out.print("DEBUG: inizio this.isSovraccarico ");

		int serviti = this.getNNodiDip();
		if(this._capac < serviti)
			return true;
		else
			return false;
	}

	/**
	 * La sua disconnessione provoca la disconnessione di almeno un Utente
	 */
	public boolean isFondamentale(){
		//System.out.println("DEBUG: inizio this.isFondamentale ");
		int ndip = this.getNNodiDip();
		//System.out.println("DEBUG: ndip "+ndip);

		for(String x: give){
			Utenti uteCor = Rete.utha.get(x);//utenteCorrente
			if(uteCor!= null){
			if(uteCor.hasMultiTra()){//e' collegato a piu di un trasmettitore
				for(String y : uteCor.take){
					Trasmettitori traCor = Rete.trha.get(y);
					if(traCor!=this&&!traCor.isSovraccarico()){//non sono io e effettivamente l'altro trasformatore lo sta alimentando
						ndip--;
						//System.out.println("DEBUG: ndip-- => "+ndip);

					}
				}
			}}
		}
		if(ndip>0){
			//System.out.println("DEBUG: fine this.isFondamentale ==>> true");
			return true;
			}
		else
			return false;
		
	}

	/**
	 * Indica il numero di Utenti dipendenti/collegati a questo Trasmettitore.
	 */
	public int getNNodiDip(){
		//System.out.println("DEBUG: inizio Trasmettitori.getNNodiDip()");
		int nUteDip = 0;
		for(String x:this.give){
			if(Rete.utha.containsKey(x)){
				nUteDip++;
			}
		}
		
		return nUteDip;
	}

	/**
	 * Valuta se il Trasmettirore e' pericoloso, cioe' se disconnesso, disconnetterebbe piu' Utenti di quelli direttamente serviti
	 * Questo e' possibile perche' un Trasmettitore sovraccarico verrebbe a sua volta disconnesso e quindi tutti i suoi Utenti.
	 */
	public boolean isPericoloso(){
		if(this.isFondamentale()){
			//se i vicini sono sovraccarichi
			for(String x : this.give){
				Utenti y = Rete.utha.get(x);
				for(String k : y.take){
					Trasmettitori h = Rete.trha.get(k);//h sono i vicini
					if(h.isSovraccarico()){
						//vicinoSovracc=true;
						return true;
					}
				}
			}
		}
		return false;
	}
}
