package soluzione;

import java.io.*;
import java.nio.file.*;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * @author Alessandro Buggin 718777
 * @author Andrea Chiericati 718984
 *
 */
public class Rete {

	static Hashtable<String,Generatori> geha = new Hashtable <String,Generatori>();			//  Generatori  Hashtable
	static Hashtable<String,Trasmettitori> trha = new Hashtable <String,Trasmettitori>();	//Trasformatori Hashtable
	static Hashtable<String,Utenti> utha = new Hashtable <String,Utenti>();					//   Utenti       " "
	static Hashtable<String,String> anha = new Hashtable<String,String>(); 					//  anomalie    HashTable
	static LinkedList<String> etgen = new LinkedList<String>();								//  etichette   Generatori
	static LinkedList<String> ettra = new LinkedList<String>();
	static LinkedList<String> etute = new LinkedList<String>();
	static LinkedList<String> etano = new LinkedList<String>();
	private static int slotGen;
	private static int slotTra;
	private static int nbrLine;
	private static BufferedReader reader;



	public static void main(String[] args) {
		Console c = System.console();
		if (c == null) {
			System.err.println("No console available.");
			System.exit(1);
		}

		String file = c.readLine("Inserisci il nome del file con le istruzioni da caricare\n(istr.txt)\n");
		try {
			caricaComandi(file);
		} catch (MalformedFileException e) {
			e.printStackTrace();
		} catch (NoSuchFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}

		return;
	}

	/**
	 * suggerisce quale connessione eliminare per massimizzare il numero di utenti connessi, eliminando il parametro.
	 */
	private static void riconfigura(String t) {
		System.out.print("DEBUG: inizio riconfigura("+t+")\n");
		Trasmettitori t1 = Rete.trha.get(t);
		if(t1.isFondamentale()){
			//System.out.println("DEBUG: this.isFondamentale");
			for(String x : t1.give){
				Utenti y = Rete.utha.get(x);
				if(y!=null){
					for(String z: y.take){
						Trasmettitori k = Rete.trha.get(z);
						if(k!=null&&k.isSovraccarico()){
							//elimina la connessione tra sovraccarico e nodo iniziale
							System.out.print("Connessione che si suggerisce di rimuovere ("+z+","+x+")\n");
							System.out.println("DEBUG:fine riconfigura");

						}
					}
				}
			}
		}else{
			System.out.println("DEBUG:fine riconfigura");
		}
	}
	
	/**
	 * controlla che la rete sia minimale:
	 * e' minimale se tutti i  generatori reggono un utente
	 */
	private static void minimale() {
		int capGenTot= getCapGenTot();
		for(String x : etgen){
			Generatori y = geha.get(x);
			if(y!=null&&(capGenTot-y._capac)>utha.size()){
				System.out.println("0");
				return;
			}
		}
		System.out.println("1");
	}
	
	/**
	 * controlla che la rete sia minima:
	 * e' minima se la capacita' complessiva di tutti i generatori della rete e' uguale al numero di utenti
	 */
	private static void minima() {
		int capGenTot= getCapGenTot();
		if(capGenTot==Rete.utha.size())
			System.out.println("1");
		else
			System.out.println("0");
	}

	/**
	 * restituisce la capacita' complessiva di tutti i <code>Generatori</code>
	 */
	private static int getCapGenTot(){
		int	capGenTot = 0;
		for(String x : etgen){
			Generatori y = geha.get(x);
			if(y!=null){
				capGenTot+=y._capac;
			}
		}
		return capGenTot;
	}

	/**
	 * Rimuove il Trasmettitore <code> t </code>se la sua eliminazione non provoca la disconnessione di alcun utente.
	 * Corrisponde a 'e' nella lista dei comandi
	 * @param t <code>Trasmettitori</code> da eliminare
	 */
	private static void elimina(String t) {
		//System.out.print("DEBUG: inizio elimina("+t+")");
		Trasmettitori t1 = Rete.trha.get(t);
		if(!t1.isFondamentale()){
			for(String x : t1.take){
				Nodi y = trha.get(x);
				if(y==null){
					y = geha.get(x);
				}
				y.give.remove(t);
				System.out.println(x.toString()+".listaConnessi.remove("+t+")");
			}
			Rete.trha.remove(t1);
			ettra.remove(t);
		}
	}
	
	/**
	 * aggiunge reciprocamente gli elementi alle proprie liste concatenate che indicano le connessioni
	 */
	private static void inserisci(String n1, String n2) throws MalformedFileException {
		if(geha.containsKey(n1)&&trha.containsKey(n2)){
			//gen,tra
			Generatori x = geha.get(n1);
			Trasmettitori y = trha.get(n2);
			x.give.addLast(n2);
			x.liberi--;
			y.take.addLast(n1);
			checkCapac(x,n1,n2);
		}
		else{
			if(trha.containsKey(n1)&&geha.containsKey(n2)){
				//tra,gen
				Trasmettitori x = trha.get(n1);
				Generatori y = geha.get(n2);
				x.take.addLast(n2);
				y.give.addLast(n1);
				y.liberi--;
				checkCapac(y,n1,n2);
			}
			else{
				if(trha.containsKey(n1)&&utha.containsKey(n2)){
					//tra,ut
					Trasmettitori x = trha.get(n1);
					Utenti y = utha.get(n2);
					x.give.addLast(n2);
					x.liberi--;
					y.take.addLast(n1);
					checkCapac(x,n1,n2);
				}
				else{
					if(utha.containsKey(n1)&&trha.containsKey(n2)){
						//ut,tra
						Utenti x = utha.get(n1);		
						Trasmettitori y = trha.get(n2);
						x.take.addLast(n2);
						y.give.addLast(n1);
						y.liberi--;
						checkCapac(y,n1,n2);
					}
					else{
						if(trha.containsKey(n1)&&trha.containsKey(n2)){
							//tra,tra
							Trasmettitori x = trha.get(n1);
							Trasmettitori y = trha.get(n2);
							x.give.addLast(n2);
							y.take.addLast(n1);
							x.liberi--;
							checkCapac(x,n1,n2);				
						}
						else{//non accettabile
							throw new MalformedFileException("Nessuna chiave disponibile per la coppia "+n1+" - "+n2);
						}
					}
				}
			}
		}
	}

	/**
	 * controlla che il nodo padre possa sostenere il figlio, altrimenti la connessione viene aggiunta alla tabella hash delle anomalie
	 * @param sopra Trasmettitore o Generatore 
	 * @param sotto 
	 */
	private static void checkCapac(Nodi x, String sopra, String sotto){
		if(x.liberi<0){
			anha.put(sotto, sopra);
			etano.add(sotto);
		}
		return;
		
	}
	
	/**
	 * Stampa a video tutti gli utenti e per ogni utente i trasmettitori collegati.
	 * Stampa a video tutti i trasmettitori e per ognuno di essi, i trasmettitori e i generatori collegati.
	 */
	private static void visualizza() throws MalformedFileException, NullPointerException{
		System.out.println("DEBUG: inizio visualizza()");

		for(String x : etute){
			System.out.println("Utenti: "+x);
			for(String y : utha.get(x).take){
				if(trha.get(y)!=null){
					System.out.println(" |\n ->"+y);
				}
			}
		}
		for(String z : ettra){
			Trasmettitori t = trha.get(z);
			System.out.println("Trasmettitori: "+z);
			for (String k : t.take ){
				if(trha.get(k)!=null||geha.get(k)!=null)
					System.out.println(" |\n ->"+k);
			}
		}
	}

	/**
	 * Stampa a video i collegamenti dell'utente con etichetta <code>utente</code>
	 * @param utente etichetta di un <code>Utenti</code>
	 */
	private static void visualizza(String utente) throws MalformedFileException{
		Utenti x = Rete.utha.get(utente);
		if(x!=null){
			System.out.print(utente+"\n |\n ->");
			for(String tras : x.take){
				Trasmettitori y = trha.get(tras);
				if(y!=null){
					visitaTras(tras);
				}
			}
		}
	}

	/**
	 * visualizza le connessioni del nodo <code>tras</code>
	 */
	private static void visitaTras(String tras) {
		Trasmettitori x = Rete.trha.get(tras);
		System.out.println(tras);
		for(String sup: x.take){
			Nodi s = trha.get(sup);
			if(s!=null){
				System.out.print("\n   |\n   -->");
				visitaTras(sup);
			}else{
				s = geha.get(sup);
				if(s!=null){					
					System.out.print("\t\t");
					visitaGen(sup);
				}
			}
		}
	}
		
	/**
	 * termina la ricorsione
	 */
	private static void visitaGen(String sup) {
		
		System.out.println("-->>"+sup);
	}

	/** 
	 * Carica la configurazione della rete, comando 'c'
	 * @param fileMap etichetta String a cui si accede per ricavare la configurazione della rete
	 */
	private static void caricaMappa(String fileMap) throws MalformedFileException, IOException{
		reader = accessoFile(fileMap);
		nbrLine = 1;
		String line=null;
		String[] arrayTemp;
		while(nbrLine<=3&&(line= reader.readLine())!=null){
			arrayTemp = line.split(" ");
			if(nbrLine==1){
				//carica etichette generatori
				for(String da : arrayTemp){
					etgen.add(da);
				}
			}else{
				if(nbrLine==2){
					//carica etichette trasmettitori
					for(String da : arrayTemp){
						ettra.add(da);
					}
				}else{
					if(nbrLine==3){
						//carica etichette utenti
						for(String da : arrayTemp){
							etute.add(da);
						}
					}
				}

			}
			nbrLine++;
		}
		int G = etgen.size();//maiuscolo perche' mantengo come sul pdf
		int T = ettra.size();//idem
		slotGen=3+G;
		slotTra=3+G+T;
		for (String x : etute){//creo gli Utenti
			utha.put(x,new Utenti());
		}
		line=reader.readLine();
		line = creaGeneratori(line);
		line = creaTrasmettitori(line);
		while(line!=null)
			line = creaConnessioni(line);
	}

	/**
	 * Inserisce nella tabella hash dei generatori tutti i generatori e le loro relative capacita
	 */
	private static String creaGeneratori(String line) throws MalformedFileException, IOException {
		if(nbrLine>=4&&nbrLine<=slotGen){
			//System.out.println("DEBUG: creaGeneratori()\nDEBUG: nbrLine\t"+nbrLine);
			//capacita' generatore deve essere un numero
			if(line.matches("\\D")){
				System.out.println("Capacita' generatore dev'essere un numero\n nbrLine="+nbrLine);
				throw new MalformedFileException(line);
			}else{
			for(String x : etgen){
				//System.out.println("DEBUG: geha.put("+x+","+"new Generatori("+line+"))");
				geha.put(x,new Generatori(Integer.parseInt(line)));
				line=reader.readLine();
				nbrLine++;
				//System.out.println("DEBUG: creaGeneratori: "x,);
			}
		}
			}
		return line;
	}

	/**
	 * controlla che la sintassi delle righe relative alle connessioni siano corrette e chiama inserisci()
	 */
	private static String creaConnessioni(String line) throws MalformedFileException, IOException {
		if(line.indexOf(' ')==-1){
		System.out.println("manca lo spazio\tnbrLine = "+nbrLine);
		throw new MalformedFileException(line);
	}
	else {
		int indice = 0;
		String n1 = line.substring(indice,line.indexOf(' '));
		indice = line.indexOf(' ');
			//System.out.println("DEBUG: indexOf(' ')="+indice);
		String n2 = line.substring(indice+1,line.length());
			//System.out.println("DEBUG: inserisci("+n1+","+n2+")");
		inserisci(n1,n2);
		line=reader.readLine();
		nbrLine++;
	}
		return line;		
	}

	/**
	 * Inserisce nella tabella hash dei trasmettitori tutti i trasmettitori e le loro relative capacita'
	 */
	private static String creaTrasmettitori(String line) throws MalformedFileException, IOException {
		if(nbrLine>=slotGen+1&&nbrLine<=slotTra){
			if(line.matches("\\D")){
				System.out.println("capacita trasmettitori dev'essere un numero\n nbrLine="+nbrLine);
				throw new MalformedFileException(line);}
					//capacita' trasmettitori
				for(String x : ettra){
					//System.out.println("DEBUG: trha.put("+x+","+"new Trasmettitori("+line+"))");
					trha.put(x,new Trasmettitori(Integer.parseInt(line)));
					line=reader.readLine();
					nbrLine++;
					//System.out.println("DEBUG: creaTrasmettitori: nbrLine++");
				}
			}
		return line;
	}

	/** 
	 * Parsing dei comandi dal file passato come parametro
	 * @param file comandi
	 */
	private static void caricaComandi(String file) throws MalformedFileException, NoSuchFileException, IOException{
		BufferedReader readerCaricaComandi=accessoFile(file);
		if(readerCaricaComandi!=null){
			String linea=null;
				if((linea=readerCaricaComandi.readLine())!= null){
					if(linea.charAt(0)!='c'){
						throw new MalformedFileException("Errore: Come prima istruzione devi caricare un file con la descrizione della rete");
					}else{
						String fileMap = linea.substring(linea.indexOf(' ')+1,linea.length());
						caricaMappa(fileMap);
						checkRete();
						while((linea=readerCaricaComandi.readLine())!=null){
							System.out.println("DEBUG:Comando letto:\t"+linea);
							if(!linea.isEmpty()){
							switch(linea.charAt(0)){
							case('v'):{
								if(linea.length()>=2){
									String utente = linea.substring(linea.indexOf(' ')+1,linea.length());
									visualizza(utente);
								}else{
									visualizza();
								}break;
							}
							case('i'):{
								String[] n = linea.split(" ");					
								inserisci(n[1],n[2]);
								break;
							}
							case('e'):{
								String t = linea.substring(linea.indexOf(' ')+1,linea.length());
								elimina(t);
								break;
							}
							case('m'):{
								switch(linea.charAt(1)){
									case('l'):{
										minimale();
										break;
									}
									case('n'):{
										minima();
										break;
									}
								}
								break;
							}
							case('r'):{
								String t = linea.substring(linea.indexOf(' ')+1,linea.length());
								riconfigura(t);
								break;
							}
							}
						}
							}
					}
				}
			
		}
		return;
	}
	
	/**
	 * Controlla che la rete sia connessa
	 */
	private static void checkRete() throws MalformedFileException {
		for(String x : etano){
			Utenti j = utha.get(x);
			if(j!=null&&j.hasMultiTra())
				for(String k : j.take){
					if(k!=anha.get(x)){
						if(anha.contains(k)){
							//possiamo cancellare la connessione dalle anomalie
							anha.remove(x);
							//etano.remove(x);
						}
					}
				}
		}
		
		if(!anha.isEmpty()||(getCapGenTot()-utha.size())<0){
			//System.out.println(anha.size());
			throw new MalformedFileException("La rete non e' connessa.");
		}
	}

	/** 
	 * Esegue i controlli sulla presenza del file
	 * @param file String che viene controllato
	 */
	private static BufferedReader accessoFile(String file) throws NoSuchFileException,IOException{
		Path fp =Paths.get(file);
		fp = fp.toRealPath();
		InputStream in = Files.newInputStream(fp);
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		return reader;
	}

}
