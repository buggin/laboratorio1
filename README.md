#Project for the first year at Insubria University.
##Related courses: Algorithm, Java, Computer Architecture.

* Problem: managment of an electrical infrastructure.
* Input: __file__ containg at the first line, the __description__ of the net (consisting in Generators, Transmitters and Users, with their capacity and relative connections).
    Successive lines contains the __command__ to execute on the loaded net.
* Output: __little__ and unmeaningful as specified by the assignment
* Grade: __28__/30
* Suggestion received: use of getter/setter, improve variables' name.