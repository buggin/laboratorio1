  .data
					 
str:                 .asciiz "Il programma lavora su una rete elettrica, costituita da generatori (max 10) e utenti (max 30); si possono calcolare tre proprieta' di tale rete: la connessione della stessa, l'indispensabilita' di un generatore e la possibilita' di ridurre la capacita' di generazione di un generatore."
str1:                .asciiz "
 
MENU PRINCIPALE (Premere la barra spaziatrice se non si visualizza l'ultima riga del programma)"
str2:                .asciiz "

Digitare 0 per -Inserisci Rete- o 1 per -Esci-.    "    
str3:                .asciiz "

MENU DI SCELTA (Premere la barra spaziatrice se non si visualizza l'ultima riga del programma)"
str4:                 .asciiz "

Digitare 0 per -Connessa-, 1 per -Generatore Indispensabile-, 2 per -Capacita Generatore- e 3 per -Torna al Menu Principale-  -->   " 
str5:                 .asciiz "
Inserire il numero di generatori presenti nella Rete:    "
str6:                 .asciiz "Inserire il numero di utenti presenti nella Rete:       "
str8:                 .asciiz "ERRORE. Valori non corretti. Reinserire i valori.   
"
str9:                 .asciiz "Inserire ora le capacita' dei generatori (andando in ordine, dal primo all'ultimo), seguite dagli utenti serviti da ciascun generatore  
"
str10:                .asciiz "Ora inserire gli utenti che ricevono energia da questo generatore. Inserire il valore 0 se il generatore non fornisce energia a uns numero di utenti uguale alla sua capacita' (esempio, se il generatore ha capacita' 3 e fornisce energia a 2 utenti, il terzo utente sara' chiamato 0).     
"
str11:                .asciiz "Inserire utente -->    "		
str12:                .asciiz "Inserire il generatore -->    "
str13:                .asciiz " 0 --> Non connessa "
str14:                .asciiz " 1 --> Connessa "
str15:                .asciiz " 0 --> Generatore non indispensabile "
str16:                .asciiz " 1 --> Generatore indispensabile "
str17:                .asciiz "La capacita' di generazione del generatore puo' essere decrementata di  "
str18:                .asciiz "Il numero dell'utente non puo' essere maggiore del numero di utenti. Reinserire il valore dell'utente. 
"
str19:                .asciiz "Digitare 0, 1, 2 o 3.
"
str20:                .asciiz "I valori non possono essere negativi. Reinserire i valori.
"
str21:                .asciiz "Inserire la capacita' del generatore -->     "
str22:                .asciiz " ----- FINE PROGRAMMA ----- "
stru:                 .space 300
		
		             .text
                     .globl main

main:                li $v0, 4
                     la $a0, str                           #INTESTAZIONE PROGRAMMA
		             syscall
		
princ:		         li $v0, 4 
                     la $a0, str1                          #MENU PRINCIPALE
		             syscall 
		
menuiniz:		     li $v0, 4
                     la $a0, str2                          #VOCI MENU PRINCIPALE
		             syscall
		
		             li $v0, 5
		             syscall                               #SCELTA DELL'UTENTE
					 
					 bgt $v0, 1 exit3                      #VALORE ERRATO
					 blt $v0, 0 exit3                      #VALORE ERRATO
		
		             beq $v0, 0 insrete                    #SE SCELTA É ZERO ALLORA VAI A INSERIMENTO RETE
					 
					 li $v0, 4
					 la $a0, str22
					 syscall
					 
exit:                li $v0, 10
                     syscall                               #ALTRIMENTI ESCE
					 
exit1:               li $v0, 4
                     la $a0, str8                          #CODICE D'ERRORE, VALORI ERRATI
		             syscall 
					 
					 j insrete
					 
exit2:               li $v0, 4
                     la $a0, str20                         #CODICE D'ERRORE, VALORI NEGATIVI
		             syscall 
					 
					 j insrete
					 
exit3:               li $v0, 4
                     la $a0, str8                          #CODICE D'ERRORE, VALORI ERRATI
		             syscall 
					 j menuiniz
					
					 


insrete:             li $v0, 4
                     la $a0, str5                          #INSERIRE IL NUMERO DI GENERATORI
		             syscall   
					 
					 li $v0, 5
		             syscall
					 
					 move $s0, $v0                         #$s0=NUMERO DI GENERATORI
					 
					 bgt $s0, 10 exit1                     #SE GENERAT. > 10 --> ESCE
					 blt $s0, 0 exit2                      #SE GENERAT. < 0 --> ESCE
					 
					 li $v0, 4
                     la $a0, str6                          #INSERIRE NUMERO UTENTI
		             syscall  
					 
					 li $v0, 5
		             syscall
					 
					 
					 move $s1, $v0                         # $s1 = NUMERO DI UTENTI
					 bgt $s1, 30 exit1  			       #SE UTENTI > 30 --> ESCE
					 blt $s1, 0 exit2                      #SE UTENTI < 0 --> ESCE
					 
                     				 
					 
					
					 
					 addi $sp, $sp, -2
					 sb $s0, 0($sp)     					# PUSH NUMERO DI GENERATORI
					 sb $s1, 1($sp)                         # PUSH NUMERO DI UTENTI
					 
					 
					 
					 
					 jal INSPROC_1
					 lb $s2, 0($sp)                        #$s2 = NUMERO TOTALE DI CELLE DI MEMORIA UTILIZZATE
					 
					
					 
						 
scelta:				 li $v0, 4
                     la $a0, str3                          #MENU DI SCELTA
		             syscall 
					 
					 li $v0, 4
                     la $a0, str4                          #VOCI MENU DI SCELTA..
		             syscall 
					 
					 li $v0, 5                             # SCELTA DELL'UTENTE
					 syscall
					 
					 bgt $v0, 3 errore
					 blt $v0, 0 errore
					 
					 beq $v0, 0 conn
					 beq $v0, 1 indis
					 beq $v0, 2 capac
					 
					 j princ                              # SE SCELTA = 3
					 
					 
errore:				 li $v0, 4
                     la $a0, str19
                     syscall
                     j scelta					 
					 
					 
					 
					 
conn:                addi $sp, $sp -2
					 sb $s2, 1($sp)						   #PUSH NUMERO TOTALE DI CELLE
					 sb $s1, 2($sp)                        #PUSH NUMERO DI UTENTI
					 
					 jal CONNESSA_2
					 
					 lbu $t0, 0($sp)
					 beq $t0, 0 stamp0
                     
					 li $v0, 4
                     la, $a0, str14
                     syscall	
                     j scelta					 
				
stamp0:              li $v0, 4
                     la $a0, str13
                     syscall
					 j scelta
					 

indis:               li $v0, 4
                     la $a0, str12                         #INSERIRE IL GENERATORE
		             syscall 
					 
					 li $v0, 5
					 syscall
					 
					 blt $v0, 0 errore1                    #VALORE NEGATIVO
					 
					
					 
					 li $s7, 0
					 
					 addi $sp, $sp -2
					 sb $s0, 1($sp)						   #PUSH NUMERO TOTALE DI GENERATORI
					 sb $v0, 2($sp)                        #PUSH GENERATORE RICHIESTO
					 
					 jal G.INDISPENSABILE_3
					 
					 beq $s7, 5 indis
					 
					 lbu $t0, 0($sp)
					 beq $t0, 0 stampa0
                     
					 li $v0, 4
                     la, $a0, str16
                     syscall	
                     j scelta					 
				
stampa0:             li $v0, 4
                     la, $a0, str15
                     syscall
					 j scelta
					 
errore1:             li $v0, 4
                     la, $a0, str20
                     syscall
					 
					 j indis
					 
					 
			
					 
					 
capac:               li $v0, 4
                     la $a0, str12                         #INSERIRE IL GENERATORE
		             syscall 
					 
					 li $v0, 5
					 syscall
					 
					 blt $v0, 0 errore2                    #VALORE NEGATIVO
					 
					 li $s7, 0
					 
					 addi $sp, $sp -2
					 sb $s0, 1($sp)						   #PUSH NUMERO TOTALE DI GENERATORI
					 sb $v0, 2($sp)                        #PUSH GENERATORE RICHIESTO
					 
					 jal CAP.GENERATORE_4
					 
					 beq $s7, 5 capac
					 
					 lbu $t0, 0($sp)
					 
					 li $v0, 4
                     la $a0, str17                         #LA CAPACITA É..
		             syscall 
					 
					 li $v0, 1
					 move $a0, $t0
					 syscall
					 
					 j scelta
					 
errore2:             li $v0, 4
                     la, $a0, str20
                     syscall
					 
					 j capac
					 
					 

					 
					 
					 
					 
					 
INSPROC_1:           lb $t0, 0($sp)                        # $t0=NUMERO DI GENERATORI
                     lb $t7, 1($sp)                        # $t7=NUMERO DI UTENTI
					 
					 la $s3, stru
					
					 la $t4, stru				   		   # $t4 = INDIRIZZO
					 li $t2, 1                             # $t2=CONTATORE GENERATORI
					 li $t3, 0                             # $t3=CONTATORE UTENTI DEI SINGOLI GENERATORI
					 
					 li $v0, 4
					 la $a0, str9                          # str9 = INSERIRE CAPACITÁ DEI GENERATORI
					 syscall
					 
					 
proc1_1:             bgt $t2, $t0 exit_1                   # exit_1= HO FINITO DI INSERIRE
                     
					 li $v0, 4
					 la $a0, str21                         # str21 = INSERIRE CAPACITÁ DEL GENERATORE
					 syscall
					 
					 li $v0, 5
					 syscall
					 
					 blt $v0, 0 exit1_1
					 
					 sb $v0, 0($s3)                        # METTO IN MEMORIA LA CAPACITÁ
					 addi $s3, $s3, 1
					 
					 move $t6, $v0                         # $t6 = CAPACITA GENERATORE ATTUALE
					 
                     li $v0, 4
					 la $a0, str10                         # str10 = ORA INSERIRE UTENTI..
					 syscall
					 
proc2_1:             beq $t3, $t6, proc3_1
                     
					 li $v0, 4
					 la $a0, str11                         # str11 = INSERIRE UTENTE
					 syscall
					 
					 li $v0, 5
					 syscall     
                     
					 blt $v0, 0 exit2_1
					 
					 bgt $v0, $t7 proc4_1
					 
					 sb $v0,  0($s3)
					 addi $s3, $s3, 1
					 addi $t3, $t3, 1
					 j proc2_1
					 
proc3_1:             addi $t2, $t2, 1
                     li $t3, 0							   # AZZERO IL CONTATORE UTENTI DEI SINGOLI GENERATORI
					 j proc1_1
					 
proc4_1:             li $v0, 4
                     la $a0, str18                         # UTENTE NON ESISTENTE  
					 syscall
					 j proc2_1
					 
exit_1:              sub $t5, $s3, $t4                     # $t5 = $s3 - $t4 --> NUMERO TOTALE DI CELLE DI MEMORIA OCCUPATE
                     addi $sp, $sp, -1
					 sb $t5, 0($sp)
                     jr $ra
					 
exit1_1:             li $v0, 4
                     la $a0, str20
					 syscall
					 
					 j proc1_1
					 
exit2_1:             li $v0, 4
                     la $a0, str20
					 syscall
					 
					 j proc2_1
				
                     
					 
					 
					 
					  
					
					 
					 
					 
					 
					 
CONNESSA_2:          lb $t1, 1($sp) 					   #$t1= NUMERO TOTALE CELLE MEMORIA OCCUPATE DALLA STRUTTURA
					 lb $t8, 2($sp)   					   # $t8 = NUMERO DI UTENTI
					 
					 la $s4, stru
					 
					 
					 li $t3, 1                             #$t3 = VALORE CHE CERCO
iniz_2:              li $t4, 0                             #$t4 = CONTATORE GENERALE
                     la $s4, stru                          #$s4 HA NUOVAMENTE IL VALORE INIZIALE (DELL'INDIRIZZO)
proc1_2:             lb $t5, 0($s4)                        #$t5 = CAPACITÁ GENERATORE
                     li $t6, 0                             #$t6 = CONTATORE UTENTI SINGOLI GENERATORI
proc2_2:             addi $s4, $s4, 1
                     lb $t7, 0($s4)                        #$t7 = VALORE CHE STO ANALIZZANDO
					 beq $t3, $t7, exit_2                  #SE HO TROVATO IL VALORE, ESCO
					 addi $t6, $t6, 1
					 addi $t4, $t4, 1
					 beq $t4, $t1, exit1_2
					 beq $t6, $t5, proc3_2
					 j proc2_2
proc3_2:             addi $s4, $s4, 1
                     j proc1_2
exit_2:              addi $t3, $t3, 1
                     bgt $t3, $t8, exit2_2
					 j iniz_2
exit1_2:             li $t9, 0                             #$t9 = BIT RISULTANTE DALLA PROCEDURA
                     addi $sp, $sp, -1
					 sb $t9, 0($sp)                             
					 jr $ra                                #RETE NON CONNESSA
exit2_2:             li $t9, 1
                     addi $sp, $sp, -1
					 sb $t9, 0($sp)
					 jr $ra                                #RETE CONNESSA                
                     
					 
					 
					 
					 
					 
					 
					 
G.INDISPENSABILE_3:  lb $t1, 1($sp) 					   #$t1= NUMERO DI GENERATORI
					 lb $t2, 2($sp)   					   # NUMERO DEL GENERATORE RICHIESTO
					 
					 la $s5, stru
					  
					 bgt $t2, $t1, exit_3                  # VALORE ERRATO	
					 li $t3, 1							   # $t3 = CONTATORE GENERATORI
					 la $t4, stru     					   # $t4 = INDIRIZZO INIZIALE
iniz_3:              lb $t5, 0($s5)                        # $t5 = CAPACITÁ GENERATORE    						     INDIVIDUO
                     beq $t2, $t3, proc_3                  # proc_3 =HO TROVATO IL GENERATORE                        IL
					 add $s5, $s5, $t5 					   # IND = IND + CAP                                         GENERATORE
					 addi $s5, $s5, 1                      # IND = IND + 1										     RICHIESTO
					 addi $t3, $t3, 1                      # INCREM. IL CONTATORE DEI GENERATORI
					 j iniz_3
					 
					                                       #ARRIVO A proc_3 CON $s5=INDIRIZZO GENERAT. RICHIESTO
					                                       #$t1=NUMERO TOTALE DI GENERATORI
								                           #$t2=$t3=NUMERO DEL GENERATORE RICHIESTO
									                       #$t4=INDIRIZZO INIZIALE
									                       #$t5=CAPACITÁ GENERATORE RICHIESTO
									   
									  
proc_3:              li $t3, 1                             # $t3 = CONTATORE GENERATORI
                     li $t6, 1                             # $t6 = CONTATORE UTENTI DEL GENERATORE RICHIESTO
proc1_3:             bgt $t6, $t5, exit1_3                 # exit1_3 = NON INDISPENSABILE (SONO RIUSCITO A FINIRE)
                     addi $s5, $s5, 1                     
					 lb $t7, 0($s5)                        # $t7 = VALORE (UTENTE DEL GENERATORE RICH.)
					 beq $t7, 0 proc1_3
proc7_3:      		 beq $t3, $t2, proc2_3 			       # proc2_3 = GENERATORE (RICH.) DA EVITARE
					 bgt $t3, $t1, exit2_3                 #exit2_3: GENERATORE INDISPENSABILE
                     li $t9, 0                             # $t9 = CONTATORE DEGLI UTENTI DEGLI ALTRI GENERATORI

proc3_3:             lb $t8, 0($t4)                        # $t8 = CAPACITÁ GENERATORE

proc6_3:             beq $t9, $t8, proc4_3                 # SE HO FINITO GLI UTENTI DI QUESTO GENERATORE --> proc4
					 addi $t4, $t4, 1 
					 lb $t0, 0($t4)                        # $t0 = UTENTI ALTRI GENERATORI
					 beq $t0, $t7, proc5_3                 # SE HO TROVATO IL VALORE --> proc5_3
					 addi $t9, $t9, 1                      # SE NO INCREMENTO CONTATORE UTENTI DEL GENERATORE
					 j proc6_3

proc5_3:             addi $t6, $t6, 1                      #INCREMENTO CONTATORE UTENTI DEL GENERATORE RICHIESTO
                     li $t3, 1   				           #RIAZZERO CONTATORE GENERATORI
					 la $t4, stru                          #RIAZZERO $t4
					 j proc1_3

proc4_3:             addi $t4, $t4, 1                      #INCREMENTO PUNTATORE
                     addi $t3, $t3, 1                      #INCREMENTO CONTATORE GENERATORI
                     j proc7_3

proc2_3:		     add $t4, $t4, $t5                     #INCREMENTO IL PUNTATORE DELLA CAPACITÁ DEL GENERATORE
					 addi $t4, $t4, 1
					 addi $t3, $t3, 1                      #INCREMENTO CONTATORE GENERATORI
					 j proc7_3
					 
exit_3:              li $s7, 5
                     li $v0, 4 
                     la $a0, str8                          #VALORI NON CORRETTI
		             syscall
					 jr $ra
					 
exit1_3:             li $t5, 0
                     addi $sp, $sp, -1
					 sb $t5, 0($sp)                        #GENERATORE NON INDISPENSABILE(RITORNA 0)
					 jr $ra
					 
exit2_3:		     li $t5, 1
                     addi $sp, $sp, -1                     #GENERATORE INDISPENSABILE(RITORNA 1)
					 sb $t5, 0($sp)
					 jr $ra	 
					 
					 
					 
					 
					 
					 
					 
CAP.GENERATORE_4:    lbu $t1, 1($sp) 					   #$t1= NUMERO DI GENERATORI
					 lbu $t2, 2($sp)                       # NUMERO DEL GENERATORE RICHIESTO
					 
					 
					 la $s6, stru
					 
                     li $v1, 0                             # $v1 = CONTATORE FINALE (CAPACITÁ CHE PUÓ ESSERE TOLTA AL GENERATORE RICHIESTO)					 
					 bgt $t2, $t1, exit_4                  # VALORE ERRATO	
					 li $t3, 1							   # $t3 = CONTATORE GENERATORI
					 la $t4, stru   					   # $t4 = INDIRIZZO INIZIALE
iniz_4:              lb $t5, 0($s6)                        # $t5 = CAPACITÁ GENERATORE    						 INDIVIDUO
                     beq $t2, $t3, proc_4                  # proc_4 =HO TROVATO IL GENERATORE                    IL
					 add $s6, $s6, $t5 					   # IND = IND + CAP                                     GENERATORE
					 addi $s6, $s6, 1                      # IND = IND + 1										 RICHIESTO
					 addi $t3, $t3, 1                      # INCREM. IL CONTATORE DEI GENERATORI
					 j iniz_4
					 
					                                       #ARRIVO A proc_4 CON $s6=INDIRIZZO GENERAT. RICHIESTO
					                                       #$t1=NUMERO TOTALE DI GENERATORI
								                           #$t2=$t3=NUMERO DEL GENERATORE RICHIESTO
									                       #$t4=INDIRIZZO INIZIALE
									                       #$t5=CAPACITÁ GENERATORE RICHIESTO
									   
									  
proc_4:              li $t3, 1                             # $t3 = CONTATORE GENERATORI
                     li $t6, 1                             # $t6 = CONTATORE UTENTI DEL GENERATORE RICHIESTO
proc1_4:             bgt $t6, $t5, exit1_4                 # exit1 = HO FINITO GLI UTENTI DEL GENERATORE RICHIESTO
                     addi $s6, $s6, 1                     
					 lb $t7, 0($s6)     				   # $t7 = VALORE (UTENTE DEL GENERATORE RICH.)
					 beq $t7, 0 proc5_4
proc7_4:      		 beq $t3, $t2, proc2_4 			       # proc2 = GENERATORE (RICH.) DA EVITARE
					 bgt $t3, $t1, proc8_4            			
                     li $t9, 0                             # $t9 = CONTATORE DEGLI UTENTI DEGLI ALTRI GENERATORI

proc3_4:             lb $t8, 0($t4)                        # $t8 = CAPACITÁ GENERATORE

proc6_4:             beq $t9, $t8, proc4_4                 # SE HO FINITO GLI UTENTI DI QUESTO GENERATORE --> proc4
					 addi $t4, $t4, 1 
					 lb $t0, 0($t4)                        # $t0 = UTENTI ALTRI GENERATORI
					 beq $t0, $t7, proc5_4                 # SE HO TROVATO IL VALORE --> proc5
					 addi $t9, $t9, 1                      # SE NO INCREMENTO CONTATORE UTENTI DEL GENERATORE
					 j proc6_4

proc5_4:             addi $t6, $t6, 1                      #INCREMENTO CONTATORE UTENTI DEL GENERATORE RICHIESTO
                     addi $v1, $v1, 1                      #INCREMENTO CONTATORE FINALE (HO TROVATO UN UTENTE CHE PUÓ ESSERE DISCONNESSO)
                     li $t3, 1   				           #RIAZZERO CONTATORE GENERATORI
					 la $t4, stru                          #RIAZZERO $t4
					 j proc1_4

proc4_4:             addi $t4, $t4, 1                      #INCREMENTO PUNTATORE
                     addi $t3, $t3, 1                      #INCREMENTO CONTATORE GENERATORI
                     j proc7_4
					 
proc8_4:             addi $t6, $t6, 1 					   #INCREMENTO CONTATORE UTENTI DEL GENERATORE RICHIESTO
                     li $t3, 1   				           #RIAZZERO CONTATORE GENERATORI
					 la $t4, stru                          #RIAZZERO $t4
					 j proc1_4

proc2_4:		     add $t4, $t4, $t5                     #INCREMENTO IL PUNTATORE DELLA CAPACITÁ DEL GENERATORE
					 addi $t4, $t4, 1
					 addi $t3, $t3, 1                      #INCREMENTO CONTATORE GENERATORI
					 j proc7_4
					 
exit_4:              li $s7, 5
                     li $v0, 4 
                     la $a0, str8                          #VALORI NON CORRETTI
		             syscall
					 jr $ra
					 
exit1_4:             addi $sp, $sp, -1                    
					 sb $v1, 0($sp)                        # RITORNO AL CHIAMANTE IL CONTATORE DELLA CAPACITÁ  
					 jr $ra						 
			 